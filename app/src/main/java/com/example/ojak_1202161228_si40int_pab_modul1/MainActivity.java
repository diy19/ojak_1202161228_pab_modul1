package com.example.ojak_1202161228_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int itungan;

    private EditText tinggiText;
    private EditText alasText;
    private TextView resultText;

    private String tinggi;
    private String alas;
    private String temp;
    private int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tinggiText = findViewById(R.id.fieldAtas);
        alasText = findViewById(R.id.fieldBawah);
    }

    public void countClick(View view) {
        tinggi = String.valueOf(tinggiText.getText());
        alas = String.valueOf(alasText.getText());

        result = Integer.parseInt(tinggi) * Integer.parseInt(alas);
        temp = Integer.toString(result);
        resultText.setText(temp);
    }
}
